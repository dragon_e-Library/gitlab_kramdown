# frozen_string_literal: true

module GitlabKramdown
  VERSION = '0.6.0'
end
